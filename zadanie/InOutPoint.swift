//
//  InOutPoint.swift
//  zadanie
//
//  Created by Michal Kovacik on 3/5/17.
//  Copyright © 2017 Michal Kovacik. All rights reserved.
//

import UIKit

class InOutPoint: NSObject {
    let startPoint: Float
    let endPoint: Float
    
    init?(startPoint: Float, endPoint: Float) {
        if startPoint >= 0 && startPoint <= 1 && endPoint >= 0 && endPoint <= 1 && startPoint < endPoint {
            self.startPoint = startPoint
            self.endPoint = endPoint
        }
        else {
            print("Invalid inout point; Must be between 0 and 1, and start point must be lower than end point.")
            return nil
        }
    }
}
