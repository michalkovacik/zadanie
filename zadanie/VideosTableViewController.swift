//
//  VideosTableViewController.swift
//  zadanie
//
//  Created by Michal Kovacik on 3/3/17.
//  Copyright © 2017 Michal Kovacik. All rights reserved.
//

import UIKit

class VideosTableViewController: UITableViewController {

    //var videosFileNames: Array<String>! = ["mr1","mr2"]
    var videosInfoList: Array<VideoInfo> = [
        VideoInfo(videoName: "mr1", inOutPoints: [InOutPoint(startPoint: 0.2, endPoint: 0.4)!]),
        VideoInfo(videoName: "mr2", inOutPoints: [InOutPoint(startPoint: 0.1, endPoint: 0.3)!,
                                                  InOutPoint(startPoint: 0.6, endPoint: 0.7)!,
                                                  InOutPoint(startPoint: 0.8, endPoint: 0.9)!]),
        VideoInfo(videoName: "mr1", inOutPoints: [])
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Videos"
        //tableView.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return videosInfoList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
        let videoInfo = videosInfoList[indexPath.row]
        var cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "DefaultCell")
        }
        cell!.textLabel?.text = videoInfo.videoName
        guard let inOutPoints = videoInfo.inOutPoints else {
            cell!.detailTextLabel?.text = "0 in-out points"
            return cell!
        }
        if inOutPoints.count == 1 {
            cell!.detailTextLabel?.text = "\(inOutPoints.count) in-out point"
        }
        else {
            cell!.detailTextLabel?.text = "\(inOutPoints.count) in-out points"
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoPlayerViewController = VideoPlayerViewController()
        videoPlayerViewController.videoInfo = videosInfoList[indexPath.row]
        navigationController?.pushViewController(videoPlayerViewController, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
