//
//  VideoInfo.swift
//  zadanie
//
//  Created by Michal Kovacik on 3/5/17.
//  Copyright © 2017 Michal Kovacik. All rights reserved.
//

import UIKit

class VideoInfo: NSObject {
    var videoName: String
    var inOutPoints: Array<InOutPoint>?
    
    init(videoName: String, inOutPoints: Array<InOutPoint>?) {
        self.videoName = videoName
        if let inOutPoints = inOutPoints {
            self.inOutPoints = inOutPoints
        }
    }
}
