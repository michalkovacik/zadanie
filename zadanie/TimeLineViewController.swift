//
//  TimeLineViewController.swift
//  zadanie
//
//  Created by Michal Kovacik on 3/5/17.
//  Copyright © 2017 Michal Kovacik. All rights reserved.
//

import UIKit

protocol TimeLineDelegate: class {
    func pointSelected(point: InOutPoint)
    func timeSelected(time: Float)
    func didLayoutSubviews()
}

class TimeLineViewController: UIViewController {
    
    private let inOutPointViewsVerticalMargin: CGFloat = 20
    
    private var sliderView: UIView!
    private var inOutPointViews: Array<UIView>!
    public var inOutPoints: Array<InOutPoint>?
    
    public weak var delegate: TimeLineDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(recognizer:)))
        view.addGestureRecognizer(tapRecognizer)
        guard let inOutPoints = inOutPoints else {
            return
        }
        guard inOutPoints.count > 0 else {
            return
        }
        print("a")
        delegate?.pointSelected(point: (inOutPoints[0]))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let superView = view.superview {
            view.frame = superView.bounds
        }
        view.backgroundColor = UIColor.black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func moveSliderTo(position: CGFloat) {
        sliderView.frame = CGRect(x: (view.frame.size.width - 2) * position, y: sliderView.frame.origin.y, width: sliderView.frame.size.width, height: sliderView.frame.size.height)
    }
    
    public func setInOutPoints(inOutPoints: Array<InOutPoint>) {
        guard let view = view else {
            assert(false, "Use function \"setInOutPoints()\" after view did load")
        }
        
        self.inOutPoints = inOutPoints
        inOutPointViews = []
        for inOutPoint in inOutPoints {
            let newInOutPointView = self.newInOutPointView(inOutPoint: inOutPoint)
            inOutPointViews.append(newInOutPointView)
            view.addSubview(newInOutPointView)
        }
        
        guard inOutPoints.count > 0 else {
            return
        }
        selectPoint(point: inOutPoints[0])
    }
    
    func viewTapped(recognizer: UITapGestureRecognizer) {
        let xPoint = Float(recognizer.location(in: view).x) / Float(view.frame.size.width)
        
        guard let inOutPoints = inOutPoints else {
            delegate?.timeSelected(time: xPoint)
            return
        }
        
        guard inOutPoints.count > 0 else {
            delegate?.timeSelected(time: xPoint)
            return
        }
        
        selectPointWith(xPoint: xPoint)
    }
    
    private func newInOutPointView(inOutPoint: InOutPoint) -> UIView {
        let newView = UIView()
        newView.frame = CGRect(x: CGFloat(view.frame.width*CGFloat(inOutPoint.startPoint)), y: inOutPointViewsVerticalMargin, width: view.frame.width*CGFloat(inOutPoint.endPoint - inOutPoint.startPoint), height: CGFloat(view.frame.height) - CGFloat(inOutPointViewsVerticalMargin*2))
        newView.backgroundColor = UIColor.gray
        return newView
    }
    
    private func selectPointWith(xPoint: Float) {
        var nearestPoint: InOutPoint!
        guard let inOutPoints = inOutPoints else {
            return
        }
        
        for inOutPoint in inOutPoints {
            
            if nearestPoint == nil {
                nearestPoint = inOutPoint
            }
            if distanceOf(point: inOutPoint, xPoint: xPoint) < distanceOf(point: nearestPoint, xPoint: xPoint) {
                nearestPoint = inOutPoint
            }
        }
        selectPoint(point: nearestPoint)
    }
    
    private func selectPoint(point: InOutPoint) {
        let indexOfPoint = inOutPoints?.index(of: point)
        var i = 0
        for inOutPointView in inOutPointViews {
            if i == indexOfPoint {
                inOutPointView.backgroundColor = UIColor.green
            }
            else {
                inOutPointView.backgroundColor = UIColor.gray
            }
            i += 1
        }
        delegate?.pointSelected(point: point)
    }
    
    private func distanceOf(point: InOutPoint, xPoint: Float) -> Float {
        if abs(point.startPoint - xPoint) < abs(point.endPoint - xPoint) {
            return abs(point.startPoint - xPoint)
        }
        else {
            return abs(point.endPoint - xPoint)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        delegate?.didLayoutSubviews()
        if sliderView == nil {
            sliderView = UIView(frame: CGRect(x: 0, y: 0, width: 2, height: view.frame.size.height))
            sliderView.backgroundColor = UIColor.white
            view.addSubview(sliderView)
        }
        else {
            sliderView.removeFromSuperview()
            view.addSubview(sliderView)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
