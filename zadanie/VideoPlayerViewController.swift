//
//  VideoPlayerViewController.swift
//  zadanie
//
//  Created by Michal Kovacik on 3/3/17.
//  Copyright © 2017 Michal Kovacik. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerViewController: UIViewController, TimeLineDelegate {
    
    public var videoInfo: VideoInfo!
    
    private var videoPlayer: AVPlayer!
    private var videoPlayerIsPlaying: Bool = false
    private var videoPlayerCurrentTime: Float {
        get {
            return Float(videoPlayer.currentTime().seconds)
        }
    }
    private var currentInOutPoint: InOutPoint?
    private var timeLineViewController: TimeLineViewController!
    private var videoPlayerTimeObserver: Any?
    
    @IBOutlet private var videoPlayerContainerView: UIView!
    @IBOutlet private var playPauseButton: UIButton!
    @IBOutlet private var timeLineContainerView: UIView!
    
    @IBAction func playPauseButtonTapped(sender: UIButton) {
        if videoPlayerIsPlaying == true {
            videoPlayer.pause()
            videoPlayerIsPlaying = false
            playPauseButton.setTitle("Play", for: .normal)
        }
        else {
            videoPlayer.play()
            videoPlayerIsPlaying = true
            playPauseButton.setTitle("Pause", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = []
        
        guard let videoInfo = videoInfo else {
            assert(false, "Error: Property \"videoInfo\" must be set before presenting controller.")
        }
        
        title = videoInfo.videoName
        let videoFileURL = URL(fileURLWithPath: Bundle.main.path(forResource: videoInfo.videoName, ofType: "mov")!)
        let videoAsset = AVAsset(url: videoFileURL)
        let videoItem = AVPlayerItem(asset: videoAsset)
        videoPlayer = AVPlayer(playerItem: videoItem)
        let videoPlayerLayer = AVPlayerLayer(player: videoPlayer)
        videoPlayerLayer.frame = videoPlayerContainerView.bounds
        
        
        videoPlayerContainerView.layer.addSublayer(videoPlayerLayer)
        
        timeLineViewController = TimeLineViewController()
        timeLineContainerView.addSubview(timeLineViewController.view)
        timeLineViewController.delegate = self
        addChildViewController(timeLineViewController)
        
        if let inOutPoints = videoInfo.inOutPoints {
            if inOutPoints.count == 0 {
                NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem, queue: nil, using: { [weak self] (_) in
                    DispatchQueue.main.async {
                        self?.videoPlayer.seek(to: kCMTimeZero)
                        self?.videoPlayer.play()
                    }
                })
            }
        }
        
        videoPlayer.play()
        videoPlayerIsPlaying = true
        
        videoPlayerTimeObserver = videoPlayer.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(0.01, Int32(NSEC_PER_SEC)), queue: DispatchQueue.main, using: { [weak self] _ in
            if let currentInOutPoint = self?.currentInOutPoint {
                if (self?.videoPlayerCurrentTime)! + 0.01 > (self?.timeInSeconds(time: currentInOutPoint.endPoint))! {
                    self?.seekTo(value: currentInOutPoint.startPoint)
                }
            }
            let duration = CGFloat((self?.videoPlayer.currentItem!.asset.duration.seconds)!)
            let currentTime = CGFloat((self?.videoPlayer.currentTime().seconds)!)
            self?.timeLineViewController.moveSliderTo(position: currentTime / duration)
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if videoPlayerTimeObserver != nil {
            videoPlayer.removeTimeObserver(videoPlayerTimeObserver!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func didLayoutSubviews() {
        if let inOutPoints = videoInfo.inOutPoints {
            timeLineViewController.setInOutPoints(inOutPoints: inOutPoints)
        }
    }
    
    func timeInSeconds(time: Float) -> Float {
        return Float((videoPlayer.currentItem?.asset.duration.seconds)!) * Float(time)
    }

    // MARK: TimeLine delegate
    
    func seekTo(value: Float) {
        let cmTime = CMTimeMakeWithSeconds(Float64(timeInSeconds(time: value)), Int32(NSEC_PER_SEC))
        videoPlayer.seek(to: cmTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
    
    func pointSelected(point: InOutPoint) {
        currentInOutPoint = point
        seekTo(value: point.startPoint)
    }
    
    func timeSelected(time: Float) {
        seekTo(value: time)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
